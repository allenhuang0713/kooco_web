export default {
  head: {
    title: "KOOCO CO.",
    meta: [
      { charset: "utf-8" },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1, shrink-to-fit=no",
      },
      { hid: "description", name: "description", content: "KOOCO" },
      { name: "keywords", content: "OS,web,android,app,application,cloud" },
      { name: "author", content: "KOOCO CO." },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "manifest",
        href: "/manifest.json",
      },
    ],
  },
  css: [],
  plugins: [
    { src: "@/plugins/vue-awesome-swiper", ssr: false },
    { src: "~plugins/ga.js", mode: "client" },
  ],
  components: true,
  buildModules: ["@nuxtjs/eslint-module"],
  modules: ["bootstrap-vue/nuxt", "vue-scrollto/nuxt"],
  build: {
    babel: { compact: true },
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/,
        });
      }
    },
  },
};
